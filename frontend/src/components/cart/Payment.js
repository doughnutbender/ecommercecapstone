import React, { Fragment } from 'react'

import MetaData from '../layout/MetaData';
import CheckoutSteps from './CheckoutSteps';

import { useDispatch, useSelector } from 'react-redux';
import { createOrder } from '../../actions/orderActions';

const Payment = ({ history }) => { 
    
    const { cartItems, shippingInfo } = useSelector(state => state.cart);
    
    const dispatch = useDispatch();

    const order = {
        orderItems: cartItems,
        shippingInfo
    }

    //save in the session storage
    const orderInfo = JSON.parse(sessionStorage.getItem('orderInfo'));
    if (orderInfo) {
        order.totalPrice = orderInfo.totalPrice
    }

    const submitHandler = async (e) => {
        e.preventDefault()

        dispatch(createOrder(order));

        history.push('/success')
    }

    return (
        <Fragment>
            <MetaData title={'Payment'} />
            <CheckoutSteps shipping confirmOrder payment />
                
              <div className="row wrapper">
                    <div className="col-10 col-lg-5">
                    <form className="shadow-lg text-center" onSubmit={submitHandler}>
                        <h5>Please prepare <strong>PHP {`${orderInfo && orderInfo.totalPrice.toLocaleString()}`} when the package arrives (COD)</strong></h5>
                        <button
                            id="pay_btn"
                            type="submit"
                            className="btn btn-block py-3"
                            >
                                Confirm
                        </button>
                
                        </form>
                    </div>
                </div>

        </Fragment>
    )
}

export default Payment;

    



